package com.techer.simplebanking.dto;

import java.util.UUID;

public record TransactionResponseDto(String status, UUID approvalCode) {
}
