package com.techer.simplebanking.dto;

import com.techer.simplebanking.entity.Transaction;

import java.time.LocalDateTime;
import java.util.List;

public record BankAccountResponseDto(String accountNumber, String owner, double balance, LocalDateTime createdDate,
                                     List<Transaction> transactions) {
}
