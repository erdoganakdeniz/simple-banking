package com.techer.simplebanking.dto;

public record BankAccountRequestDto(String accountNumber,String owner,double balance) {
}
