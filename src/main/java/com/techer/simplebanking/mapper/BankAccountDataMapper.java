package com.techer.simplebanking.mapper;

import com.techer.simplebanking.dto.BankAccountRequestDto;
import com.techer.simplebanking.dto.BankAccountResponseDto;
import com.techer.simplebanking.entity.BankAccount;
import org.springframework.stereotype.Component;

@Component
public class BankAccountDataMapper {


    public BankAccount bankAccountRequestToBankAccount(BankAccountRequestDto bankAccountRequestDto){
        return new BankAccount.Builder()
                .accountNumber(bankAccountRequestDto.accountNumber())
                .owner(bankAccountRequestDto.owner())
                .balance(bankAccountRequestDto.balance())
                .build();
    }
    public BankAccountResponseDto bankAccountToBankAccountResponse(BankAccount bankAccount){
        return new BankAccountResponseDto(bankAccount.getAccountNumber(),
                bankAccount.getOwner(),
                bankAccount.getBalance(),
                bankAccount.getCreatedDate(),
                bankAccount.getTransactions());
    }


}
