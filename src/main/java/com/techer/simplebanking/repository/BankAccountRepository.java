package com.techer.simplebanking.repository;

import com.techer.simplebanking.entity.BankAccount;
import com.techer.simplebanking.service.BankAccountService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, UUID> {
    boolean existsBankAccountByAccountNumber(String accountNumber);
    Optional<BankAccount> findByAccountNumber(String accountNumber);
}
