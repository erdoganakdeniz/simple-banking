package com.techer.simplebanking.service;

import com.techer.simplebanking.entity.BankAccount;
import com.techer.simplebanking.entity.Transaction;
import com.techer.simplebanking.exception.AlreadyExistAccountException;
import com.techer.simplebanking.exception.NotFoundException;
import com.techer.simplebanking.repository.BankAccountRepository;
import com.techer.simplebanking.repository.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BankAccountServiceImpl implements BankAccountService{

    private final BankAccountRepository bankAccountRepository;
    private final TransactionRepository transactionRepository;

    public BankAccountServiceImpl(BankAccountRepository bankAccountRepository, TransactionRepository transactionRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public BankAccount createBankAccount(BankAccount bankAccount) {
        if (bankAccountRepository.existsBankAccountByAccountNumber(bankAccount.getAccountNumber())) {
            throw new AlreadyExistAccountException("An account with the same account number already exists: "+bankAccount.getAccountNumber());
        }
        return bankAccountRepository.save(bankAccount);
    }

    @Override
    public List<BankAccount> getAllBankAccount() {
        return bankAccountRepository.findAll();
    }

    @Override
    public BankAccount getBankAccountByAccountNumber(String accountNumber) {
        return bankAccountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(()-> new NotFoundException("Account not found with given account number: "+accountNumber));
    }

    @Override
    public UUID credit(String accountNumber,Transaction transaction) {
        BankAccount account=getBankAccountByAccountNumber(accountNumber);
        account.post(transaction);
        transaction.setAccount(account);
        transaction.setApprovalCode(UUID.randomUUID());

        transactionRepository.save(transaction);
        bankAccountRepository.save(account);
        return transaction.getApprovalCode();
    }

    @Override
    public UUID debit(String accountNumber, Transaction transaction) {
        BankAccount account=getBankAccountByAccountNumber(accountNumber);
        account.post(transaction);
        transaction.setAccount(account);
        transaction.setApprovalCode(UUID.randomUUID());
        transactionRepository.save(transaction);
        bankAccountRepository.save(account);
        return transaction.getApprovalCode();

    }
}
