package com.techer.simplebanking.service;

import com.techer.simplebanking.entity.BankAccount;
import com.techer.simplebanking.entity.Transaction;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BankAccountService {

    BankAccount createBankAccount(BankAccount bankAccount);
    List<BankAccount> getAllBankAccount();
    BankAccount getBankAccountByAccountNumber(String accountNumber);

    UUID credit(String accountNumber,Transaction transaction);
    UUID debit(String accountNumber,Transaction transaction);


}
