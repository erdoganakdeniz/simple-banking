package com.techer.simplebanking.exception;

public class AlreadyExistAccountException extends RuntimeException {
    public AlreadyExistAccountException(String message) {
        super(message);
    }
}
