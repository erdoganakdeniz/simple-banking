package com.techer.simplebanking.controller;

import com.techer.simplebanking.dto.BankAccountRequestDto;
import com.techer.simplebanking.dto.BankAccountResponseDto;
import com.techer.simplebanking.dto.TransactionResponseDto;
import com.techer.simplebanking.entity.DepositTransaction;
import com.techer.simplebanking.entity.WithdrawalTransaction;
import com.techer.simplebanking.mapper.BankAccountDataMapper;
import com.techer.simplebanking.service.BankAccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/account")
public class BankAccountController {

    private BankAccountService bankAccountService;
    private BankAccountDataMapper bankAccountDataMapper;

    public BankAccountController(BankAccountService bankAccountService, BankAccountDataMapper bankAccountDataMapper) {
        this.bankAccountService = bankAccountService;
        this.bankAccountDataMapper = bankAccountDataMapper;
    }
    @GetMapping
    ResponseEntity<List<BankAccountResponseDto>> getAllBankAccount(){
        return ResponseEntity.ok(bankAccountService.getAllBankAccount().stream()
                .map(bankAccountDataMapper::bankAccountToBankAccountResponse).collect(Collectors.toList()));
    }

    @PostMapping
    ResponseEntity<BankAccountResponseDto> createBankAccount(@RequestBody BankAccountRequestDto bankAccountRequestDto){
        return ResponseEntity.ok(bankAccountDataMapper.bankAccountToBankAccountResponse(bankAccountService.createBankAccount(bankAccountDataMapper.bankAccountRequestToBankAccount(bankAccountRequestDto))));
    }
    @GetMapping("/{account-number}")
    ResponseEntity<BankAccountResponseDto> getBankAccountByAccountNumber(@PathVariable("account-number") String accountNumber ){
        return ResponseEntity.ok(bankAccountDataMapper.bankAccountToBankAccountResponse(bankAccountService.getBankAccountByAccountNumber(accountNumber)));
    }

    @PostMapping("/debit/{account-number}")
    ResponseEntity<TransactionResponseDto> debit(@PathVariable("account-number") String accountNumber, @RequestBody double amount){
        TransactionResponseDto responseDto=new TransactionResponseDto("OK",bankAccountService.debit(accountNumber,new WithdrawalTransaction(amount)));
        return ResponseEntity.ok(responseDto);
    }

    @PostMapping("/credit/{account-number}")
    ResponseEntity<TransactionResponseDto> credit(@PathVariable("account-number") String accountNumber, @RequestBody double amount){
        TransactionResponseDto responseDto=new TransactionResponseDto("OK",bankAccountService.credit(accountNumber,new DepositTransaction(amount)));
        return ResponseEntity.ok(responseDto);
    }

}
