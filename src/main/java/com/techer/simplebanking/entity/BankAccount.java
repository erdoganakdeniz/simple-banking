package com.techer.simplebanking.entity;

import com.techer.simplebanking.exception.InsufficientBalanceException;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "bank_accounts")
public class BankAccount {

    @GeneratedValue(strategy = GenerationType.UUID)
    @Id
    private UUID id;
    @Column(name = "account_number")
    private String accountNumber;
    @Column(name = "account_owner")
    private String owner;
    @Column(name = "balance")
    private double balance;
    @CreationTimestamp
    @Column(name = "created_date")
    private LocalDateTime createdDate;
    @OneToMany(mappedBy = "account",cascade = CascadeType.ALL)
    private List<Transaction> transactions;


    public void post(Transaction transaction){

        if (transaction instanceof DepositTransaction){
            credit(transaction.getAmount());
        }
        else if (transaction instanceof WithdrawalTransaction) {
            debit(transaction.getAmount());
        }
        else if (transaction instanceof PhoneBillPaymentTransaction) {
            debit(transaction.getAmount());
        }
        transactions.add(transaction);

    }
    public void credit(double amount){
        balance+=amount;
    }

    public void debit(double amount){
        if (balance>=amount){
            balance-=amount;
        }else {
            throw new InsufficientBalanceException("Insufficient balance");
        }

    }

    public BankAccount() {
    }
    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
        this.transactions = new ArrayList<>();
    }

    public BankAccount(UUID id, String accountNumber, String owner, double balance, LocalDateTime createdDate, List<Transaction> transactions) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.owner = owner;
        this.balance = balance;
        this.createdDate = createdDate;
        this.transactions = transactions;
    }

    private BankAccount(Builder builder) {
        setId(builder.id);
        setAccountNumber(builder.accountNumber);
        setOwner(builder.owner);
        setBalance(builder.balance);
        setCreatedDate(builder.createdDate);
        setTransactions(builder.transactions);
    }
    public BankAccount(String accountNumber, String owner, double balance, LocalDateTime createdDate, List<Transaction> transactions) {
        this.accountNumber = accountNumber;
        this.owner = owner;
        this.balance = balance;
        this.createdDate = createdDate;
        this.transactions = transactions;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public static final class Builder {
        private UUID id;
        private String accountNumber;
        private String owner;
        private double balance;
        private LocalDateTime createdDate;
        private List<Transaction> transactions;

        public Builder() {
        }

        public Builder id(UUID val) {
            id = val;
            return this;
        }

        public Builder accountNumber(String val) {
            accountNumber = val;
            return this;
        }

        public Builder owner(String val) {
            owner = val;
            return this;
        }

        public Builder balance(double val) {
            balance = val;
            return this;
        }

        public Builder createdDate(LocalDateTime val) {
            createdDate = val;
            return this;
        }

        public Builder transactions(List<Transaction> val) {
            transactions = val;
            return this;
        }

        public BankAccount build() {
            return new BankAccount(this);
        }
    }
}
