package com.techer.simplebanking.entity;


import jakarta.persistence.Entity;

@Entity
public class WithdrawalTransaction extends Transaction{
    public WithdrawalTransaction(double amount) {
        super(amount);
        super.setType("WithDrawalTransaction");
    }

    public WithdrawalTransaction() {

    }
}
