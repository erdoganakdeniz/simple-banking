package com.techer.simplebanking.entity;

public class PhoneBillPaymentTransaction extends BillPaymentTransaction{
    private String phoneNumber;

    public PhoneBillPaymentTransaction(String payee, double amount,String phoneNumber) {
        super(payee, amount);
        super.setType("PhoneBillPaymentTransaction");
        this.phoneNumber=phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
