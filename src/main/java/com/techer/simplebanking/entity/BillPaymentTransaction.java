package com.techer.simplebanking.entity;

import jakarta.persistence.Entity;

@Entity
public class BillPaymentTransaction extends Transaction{

    private String payee;

    public BillPaymentTransaction(String payee,double amount) {
        super(amount);
        super.setType("BillPaymentTransaction");
        this.payee=payee;
    }

    public BillPaymentTransaction() {

    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    @Override
    public String toString() {
        return "BillPaymentTransaction{" +
                "payee='" + payee + '\'' +
                '}';
    }
}
