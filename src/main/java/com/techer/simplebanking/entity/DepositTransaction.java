package com.techer.simplebanking.entity;

import jakarta.persistence.Entity;

@Entity
public class DepositTransaction extends Transaction{

    public DepositTransaction(double amount) {
        super(amount);
        super.setType("DepositTransaction");
    }

    public DepositTransaction() {

    }
}
