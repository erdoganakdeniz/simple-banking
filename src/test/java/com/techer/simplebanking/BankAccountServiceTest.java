package com.techer.simplebanking;

import com.techer.simplebanking.entity.*;
import com.techer.simplebanking.repository.BankAccountRepository;
import com.techer.simplebanking.repository.TransactionRepository;
import com.techer.simplebanking.service.BankAccountService;
import com.techer.simplebanking.service.BankAccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class BankAccountServiceTest {


    private BankAccountRepository bankAccountRepository=mock(BankAccountRepository.class);

    private TransactionRepository transactionRepository=mock(TransactionRepository.class);

    private BankAccountService bankAccountService;

    @BeforeEach
    public void setUp(){
        bankAccountService=new BankAccountServiceImpl(bankAccountRepository,transactionRepository);
    }

    @Test
    public void testPostTransaction(){
        BankAccount account = new BankAccount("Jim",0);
        account.post(new DepositTransaction(1000));
        account.post(new WithdrawalTransaction(200));
        account.post(new PhoneBillPaymentTransaction("Vodafone", 96.50, "5423345566"));
        assertEquals(account.getBalance(), 703.50, 0.0001);
    }

    @Test
    public void testCreateBankAccount() {
        BankAccount account = new BankAccount("Jim",12345);
        when(bankAccountRepository.save(account)).thenReturn(account);
        BankAccount createdBankAccount=bankAccountService.createBankAccount(account);
        assertNotNull(createdBankAccount);
        assertEquals("Jim",createdBankAccount.getOwner());
        assertEquals(12345,createdBankAccount.getBalance());

    }

}
